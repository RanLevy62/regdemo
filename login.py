import webapp2
import os
import jinja2
import cgi
import helpers
from usersmanager import UsersManager
from cookie_manager import CookieManager

template_env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.getcwd()))

class MainPage(webapp2.RedirectHandler):
    def writeResponse(self,response,templateName,context):
        template = template_env.get_template(templateName)
        response.out.write(template.render(context))
    
    def get(self):
        if self.request.get('log_out') =='Logout':
            self.response.delete_cookie('user_id')
        else:
            cookiemanager=CookieManager()
            user_id, user = cookiemanager.resolve_cookie(self)
            if user_id : self.redirect("/tasks")

        template = template_env.get_template('login.html')
        context = {'title':'this is the login title'
                   }
        self.response.out.write(template.render(context))

    def post(self):

        helper = helpers.Helpers()
        message=''

        #get user name and password
        username=self.request.get('username')
        password= self.request.get('password')
        
        #check that user name is a valid email
        if not helper.isEmailValid(username):
            #ask for login again
            message='User name should be a valid email address'
            templateName='login.html'
            context = {'title':'login again', 'message':message,'username':username,}
            self.writeResponse(self.response,templateName, context)
            return

        #check password validity
        ''' no need to check
        if not helper.isPasswordValid(password):
             message='Password should have at least 8 characters long'
             templateName='login.html'
             context = {'title':'login again', 'message':message,'username':username}
             self.writeResponse(self.response,templateName, context)
             return
        '''
        #autonticate user
        usersmanager=UsersManager()
        user=usersmanager.validateUser(username,password)
        if user:
            self.response.set_cookie('user_id', username,)
            self.redirect('/tasks')
        else:
             message='User name or password don\'t match - please try again'
             templateName='login.html'
             context = {'title':'login again', 'message':message,'username':username}
             self.writeResponse(self.response,templateName, context)            
     
application = webapp2.WSGIApplication([('/',MainPage)],
                                      debug=True)
