import webapp2
import os
import jinja2
from usersmanager import UsersManager
from cookie_manager import CookieManager
from google.appengine.datastore.datastore_query import Cursor

template_env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.getcwd()))
PAGE_SIZE=2

class AdminPage(webapp2.RedirectHandler):
    
    def get(self):

        # Test if this is a valid user, and if not - redirect to login
        # If this is a valid user, get user_id and user_name
        cookiemanager=CookieManager()
        user_id, user = cookiemanager.resolve_cookie(self)
        if not user_id:
            self.redirect("/")
            return


        usersmanager= UsersManager()
        cursor = Cursor(urlsafe=self.request.get('cursor'))
        query_results,next_cursor,more=usersmanager.getUsersWithCurser(cursor,PAGE_SIZE)
        results= query_results['results'];
        context = {
            'admin':user.admin,
            'users': results,
        }
        if more:
            context['next_cursor'] = next_cursor.urlsafe()
        template = template_env.get_template('admin.html')
        self.response.out.write(template.render(context))


        
application = webapp2.WSGIApplication([('/admin',AdminPage)],
                                      debug=True)
