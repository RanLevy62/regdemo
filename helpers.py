import re

class Helpers:
    def isPasswordValid(self,password):
        if len(password)<6:
            return False
        return True

    def isEmailValid(self,email):
        #validate email string
        p =re.compile('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$')
        valid=p.match(email)
        if not valid:
            return False
        return True
