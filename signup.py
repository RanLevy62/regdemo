import webapp2
import os
import jinja2
import helpers
from datetime import date
from usersmanager import UsersManager
from usersmanager import UsersManagementError
from user import User

template_env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.getcwd()))

class SignupPage(webapp2.RedirectHandler):
    def writeResponse(self,response,templateName,context):
        template = template_env.get_template(templateName)
        response.out.write(template.render(context))
            
    def get(self):
        cookie_value = self.request.cookies.get('user_id')
        print cookie_value
        template = template_env.get_template('signup.html')
        context = {
                   }
        self.response.out.write(template.render(context))

    def post(self):
        helper = helpers.Helpers()
        message=''

        #get user name and password
        name=self.request.get('name')
        username=self.request.get('username')
        password= self.request.get('password')
        passwordretype= self.request.get('passwordretype')
        birthyear= self.request.get('birthyear')

        #check that name is valid
        if len(name) <1 or len(name)>30:
             message='name should be between 1 and 30 characters'
             templateName='signup.html'
             context = {'title':'login again', 'message':message,'username':username,'birthyear':birthyear,'name':name}
             self.writeResponse(self.response,templateName, context)
             return
        #check that birth year is legal i.e an integer between 1900 and this this year minus 14
        try:
            val = int(birthyear)
        except ValueError:
             message='Birth year should be a number'
             templateName='signup.html'
             context = {'title':'login again', 'message':message,'username':username,'birthyear':birthyear,'name':name}
             self.writeResponse(self.response,templateName, context)
             return
        thisyear = date.today().year   
        if not (int(birthyear)>1900 and int(birthyear)<thisyear-14):
             message='Birth year not leagal'
             templateName='signup.html'
             context = {'title':'login again', 'message':message,'username':username,'birthyear':birthyear,'name':name}
             self.writeResponse(self.response,templateName, context)
             return
       
        #check that user name is a valid email
        if not helper.isEmailValid(username):
            message='User name should be a valid email address'
            templateName='signup.html'
            context = {'title':'login again', 'message':message,'username':username,'birthyear':birthyear,'name':name}
            self.writeResponse(self.response,templateName, context)
            return

        #check password validity
        if not helper.isPasswordValid(password):
             message='Password should have at least 6 characters long'
             templateName='signup.html'
             context = {'title':'login again', 'message':message,'username':username,'birthyear':birthyear,'name':name}
             self.writeResponse(self.response,templateName, context)
             return

        #check that second password equal to the first
        if not password==passwordretype:
             message='Passwords don\'t match - try again'
             templateName='signup.html'
             context = {'title':'login again', 'message':message,'username':username,'birthyear':birthyear,'name':name}
             self.writeResponse(self.response,templateName, context)
             return

        #insert the user to the datastore
        try:
            usr=UsersManager();
            usr.addUser(username,password,name,birthyear)
        except UsersManagementError as e:
            if e.errorName()=='UserAllreadyExists':   # if user allready exists
                message='This user name is allready in use - try again or go to login'
                templateName='signup.html'
                context = {'title':'login again', 'message':message,'username':username,'birthyear':birthyear,'name':name}
                self.writeResponse(self.response,templateName, context)
                return

        #user sign up good - proceed to tasks
        self.response.set_cookie('user_id', username,)
        self.redirect("/tasks")
         
application = webapp2.WSGIApplication([('/signup',SignupPage)],
                                      debug=True)
