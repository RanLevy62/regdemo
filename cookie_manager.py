from usersmanager import UsersManager

class CookieManager():
    
    def resolve_cookie(self,page):
        cookie_value = page.request.cookies.get('user_id')
        if not cookie_value:
            return (0,0)
        usermanager=UsersManager()
        user = usermanager.getUser(cookie_value)
        return (cookie_value,user)
