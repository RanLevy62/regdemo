from google.appengine.ext import ndb
from user import User

class UsersManagementError(Exception):
     def __init__(self, value):
         self.value = value
     def __str__(self):
         return repr(self.value)
     def errorName(self):
         return self.value

class UsersManager():
    def getUser(self,userId):
        key = ndb.Key(User,userId)
        obj=key.get()
        return obj

    def validateUser(self,user_id,password):
        #checks if user_id is in the store and if so, if password is correct
        #return: User object if both conditions are ture and None otherwise
        user=self.getUser(user_id)
        if not user:
            return None
        if user.password != password:
            return None
        return user
        

    def getUsersWithCurser(self,cursor,pagesize):
        #get Users objects from the store starting at cursor
        #return: User object if both conditions are ture and None otherwise
        results,next_cursor,more = User.query().fetch_page(pagesize,start_cursor=cursor)
        if not results:
            results = []
        
        context = {
        'results': results,
        }
        return context,next_cursor,more
        
        #ccc
        obj=User()
        query = obj.all()

        if cursor:
            query.with_cursor(cursor) 
        results = query.fetch(pagesize)
        if not results: results = []
        new_cursor = query.cursor()
        query.with_cursor(new_cursor)
        has_more_results = query.count(1) == 1
        context = {
        'results': results,
        }
        if has_more_results:
            context['next_cursor'] = new_cursor
        return context
        

        
    def addUser(self,userId,password,name,birthyear,admin=False):
        #look if user is already registered
        obj = self.getUser(userId)
        #if user is registered - raise exception
        if obj:
            raise UsersManagementError("UserAllreadyExists")
            return
                 
        #TODO add sanity checks
        #the key is the userId for future extraction 
        obj = User(id=userId)
        obj.name=name
        obj.password=password
        obj.birthyear=int(birthyear)
        obj.admin=admin
        a=obj.put()
        print a

    def deleteUser(self,userId):
        key = ndb.query('User', userId)
        key.delete()
