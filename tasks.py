import webapp2
import os
import jinja2
import datetime
from cookie_manager import CookieManager

from google.appengine.ext import ndb

class Task(ndb.Model):
    user_id = ndb.StringProperty(required=True)
    task = ndb.StringProperty(required=True)
    due_date = ndb.DateProperty()
    is_complete = ndb.BooleanProperty()
    
    @classmethod
    def query_task(cls,ancestor_key):
        return cls.query(ancestor=ancestor_key)
    

def get_all_user_tasks(user_id):

    ancester_key=ndb.Key("UserTasksGroup",user_id)
    return Task.query_task(ancester_key).fetch(10)

template_env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.getcwd()))

def validate_user(self):
    # Test if this is a valid user, and if not - redirect to login
    # If this is a valid user, get user_id and user_name
    cookiemanager=CookieManager()
    user_id, user = cookiemanager.resolve_cookie(self)
    if not user_id:
        self.redirect("/")
        return
    return user_id,user
            
class TasksPage(webapp2.RedirectHandler):
 
    def get(self):

        user_id,user = validate_user(self)
        
        user_name = user.name
        error_message = ""
        
        task_list = get_all_user_tasks(user_id)
        template = template_env.get_template('tasks.html')
        context = {
                   'user_name' : user_name,
                   'isUserAdmin': user.admin,
                   'error_message' : error_message,
                   'task_list' : task_list
                   }
        self.response.out.write(template.render(context))
        
    def post(self):

        user_id,user = validate_user(self)

        user_name = user.name
        error_message = ""
        
        is_add = self.request.POST.get("add_task",None)
        if(is_add):
            task = self.request.POST.get("task")
            new_task = Task(
                            parent = ndb.Key('UserTasksGroup',user_id),
                            user_id=user_id,
                            task=task)
            
            due_date = self.request.POST.get("due_date",None)
            if (due_date):
                new_task.due_date = datetime.datetime.strptime(due_date,"%d/%m/%y").date()
            new_task.put()
        
        is_delete = self.request.POST.get("delete_task",None)
        if(is_delete):
            key1 = self.request.POST.get("key_to_delete")
            key = ndb.Key(urlsafe=key1)
            key.delete()      

        self.redirect('/tasks')
            


        
application = webapp2.WSGIApplication([('/tasks',TasksPage)],
                                      debug=True)
